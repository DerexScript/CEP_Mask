<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>CEP Mask</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<h2>O campo de cep so vai aceitar apenas 8 digitos. O traço que separa o cep é opcional. </h2>
	<h3>
		Quando o usuario preecher corretamente o cep. o campo vai ganhar uma cor verde. E caso o usuario apague algum numero, deixando o formato do Cep fora das regras, o campo volta a ficar normal
	</h3>
	<form action="#" method="post" accept-charset="utf-8">
		<p>
			<label>Cep: </label>
			<input type="name" name="name" id="name" maxlength="9" value="" onBlur="verifycep();"> 
			<label id="estado" style="color: red;"></label>
		</p>
		<input type="submit" name="send" value="Enviar">
	</form>

	<script>
		function verifycep(){
			let RegExp = /[0-9]{5}-?[0-9]{3}/;
			var CampoCep = document.getElementById("name").value;
				if (document.getElementById("name").value.length > 0) {
					if (!RegExp.test(CampoCep)) {
						document.getElementById("estado").style.color = "Red";
						document.getElementById("estado").innerHTML = " *Cep Invalido";
						document.getElementById('name').style.border = null;
						
					}else{
						document.getElementById("estado").innerHTML = "";
						document.getElementById('name').style.border = 'solid 2px #46C646';
						
					}
				}else{
					document.getElementById("estado").style.color = "Red";
					document.getElementById("estado").innerHTML = " *Campo Obrigatório";	
					document.getElementById('name').style.border = null;
					
				}
		}
	</script>
</body>
</html>
